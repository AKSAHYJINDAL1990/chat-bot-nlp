## interactive_story_1
* greet
    - utter_greet
* restaurant_search
    - utter_ask_location
* restaurant_search{"location": "bangalore"}
    - slot{"location": "bangalore"}
    - action_validate_city
    - slot{"location": "bangalore"}
    - slot{"location_ok": true}
    - utter_ask_cuisine
* restaurant_search{"cuisine": "chinese"}
    - slot{"cuisine": "chinese"}
    - action_validate_cuisine
    - slot{"cuisine": "chinese"}
    - slot{"cuisine_ok": true}
    - utter_ask_budget
* restaurant_search{"budgetmax": "300"}
    - slot{"budgetmax": "300"}
    - action_search_restaurants
    - slot{"location": "bangalore"}
    - slot{"cuisine": "chinese"}
    - slot{"no_restaurant_found": false}
    - utter_send_email
* send_email
    - action_send_email
    - utter_goodbye
## interactive_story_2
* greet
    - utter_greet
* restaurant_search{"location": "Rishikesh"}
    - slot{"location": "Rishikesh"}
    - action_validate_city
    - slot{"location": "rishikesh"}
    - slot{"location_ok": false}
* restaurant_search{"location": "Allahabad"}
    - slot{"location": "Allahabad"}
    - action_validate_city
    - slot{"location": "allahabad"}
    - slot{"location_ok": true}
    - utter_ask_cuisine
* restaurant_search{"cuisine": "chinese"}
    - slot{"cuisine": "chinese"}
    - action_validate_cuisine
    - slot{"cuisine": "chinese"}
    - slot{"cuisine_ok": true}
    - utter_ask_budget
* restaurant_search{"budgetmax": "300"}
    - slot{"budgetmax": "300"}
    - action_search_restaurants
    - utter_send_email
* send_email{"email": "xyz@sth.edu"}
    - slot{"email": "xyz@sth.edu"}
    - action_send_email
    - reset_slots
    - utter_goodbye

## interactive_story_1
* greet
    - utter_greet
* restaurant_search{"location": "kolkata"}
    - slot{"location": "kolkata"}
    - action_validate_city
    - slot{"location": "kolkata"}
    - slot{"location_ok": true}
    - utter_ask_cuisine
* restaurant_search{"cuisine": "chinese"}
    - slot{"cuisine": "chinese"}
    - action_validate_cuisine
    - slot{"cuisine": "chinese"}
    - slot{"cuisine_ok": false}
    - utter_ask_budget
* restaurant_search{"budgetmax": "300"}
    - slot{"budgetmax": "300"}
    - action_search_restaurants
    - slot{"location": "kolkata"}
    - slot{"cuisine": "chinese"}
    - slot{"no_restaurant_found": true}
    - utter_ask_budget
* restaurant_search{"budgetmin": "300", "budgetmax": "700"}
    - slot{"budgetmax": "700"}
    - slot{"budgetmin": "300"}
    - action_search_restaurants
    - slot{"location": "kolkata"}
    - slot{"cuisine": "chinese"}
    - slot{"no_restaurant_found": false}
    - utter_send_email
* affirm
    - utter_ask_email
* send_email{"email": "jddk.2jmd@kdl.co.in"}
    - slot{"email": "jddk.2jmd@kdl.co.in"}
    - action_send_email
    - reset_slots
    - utter_goodbye

## interactive_story_1
* greet
    - utter_greet
* restaurant_search
    - utter_ask_location
* restaurant_search{"location": "mubaim"}
    - slot{"location": "mubaim"}
    - action_validate_city
    - slot{"location": "mubaim"}
    - slot{"location_ok": false}
* restaurant_search{"location": "mumbai"}
    - slot{"location": "mumbai"}
    - action_validate_city
    - slot{"location": "mumbai"}
    - slot{"location_ok": true}
    - utter_ask_cuisine
* restaurant_search{"cuisine": "North Indian"}
    - slot{"cuisine": "North Indian"}
    - action_validate_cuisine
    - slot{"cuisine": "north indian"}
    - slot{"cuisine_ok": false}
    - utter_ask_budget
* restaurant_search{"budgetmin": "300", "budgetmax": "700"}
    - slot{"budgetmax": "700"}
    - slot{"budgetmin": "300"}
    - action_search_restaurants
    - slot{"location": "mumbai"}
    - slot{"cuisine": "north indian"}
    - slot{"no_restaurant_found": false}
    - utter_send_email
* affirm
    - utter_ask_email
* send_email{"email": "jddk.2jmd@kdl.co.in"}
    - slot{"email": "jddk.2jmd@kdl.co.in"}
    - action_send_email
    - reset_slots
    - utter_goodbye

## interactive_story_1
* greet
    - utter_greet
* restaurant_search{"cuisine": "chinese", "location": "chandigarh"}
    - slot{"cuisine": "chinese"}
    - slot{"location": "chandigarh"}
    - action_validate_city
    - slot{"location": "chandigarh"}
    - slot{"location_ok": true}
    - action_validate_cuisine
    - slot{"cuisine": "chinese"}
    - slot{"cuisine_ok": true}
    - utter_ask_budget
* restaurant_search{"budgetmin": "300", "budgetmax": "700"}
    - slot{"budgetmax": "700"}
    - slot{"budgetmin": "300"}
    - action_search_restaurants
    - slot{"location": "chandigarh"}
    - slot{"cuisine": "chinese"}
    - slot{"no_restaurant_found": false}
    - utter_send_email
* deny
    - utter_goodbye

## interactive_story_1
* greet
    - utter_greet
* restaurant_search{"cuisine": "chinies", "location": "delhi", "budgetmin": "300", "budgetmax": "500"}
    - slot{"budgetmax": "500"}
    - slot{"budgetmin": "300"}
    - slot{"cuisine": "chinies"}
    - slot{"location": "delhi"}
    - action_validate_city
    - slot{"location": "delhi"}
    - slot{"location_ok": true}
    - action_validate_cuisine
    - slot{"cuisine": "chinies"}
    - slot{"cuisine_ok": true}
    - action_search_restaurants
    - slot{"location": "delhi"}
    - slot{"cuisine": "chinies"}
    - slot{"no_restaurant_found": false}
    - utter_send_email
* deny
    - utter_goodbye
