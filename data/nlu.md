## intent:affirm
- yes
- yep
- yeah
- indeed
- that's right
- ok
- great
- right, thank you
- correct
- great choice
- sounds really good
- thanks
- yes. please
- yes. Please

## intent:deny
- no
- definitely not
- never
- absolutely not
- i don't think so
- no sir
- no ma'am
- no way
- no sorry
- No, not really.
- nah not for me
- nah
- no and no again
- no go
- no thanks
- decline
- deny
- i decline
- never mind
- I'm not giving you my email address
- no i don't accept
- no!!!!
- no you did it wrong
- no i can't
- i'm not sure
- i don't want to
- i don't want either of those
- nah thanks
- Nope
- nope
- no. thanks

## intent:goodbye
- bye
- goodbye
- good bye
- stop
- end
- farewell
- Bye bye
- have a good one

## intent:greet
- hey
- howdy
- hey there
- hello
- hi
- good morning
- good evening
- dear sir
- Hello
- Hi

## intent:location_not_found
- Sorry, didn’t find any such location. Can you please tell again?

## intent:send_email
- yes. Please send it to [xyz@sth.edu](email)
- [jddk.2jmd@kdl.co.in](email)
- It is [test@gmail.com](email)
- my email is p[jsdd_ff32@gmail.com](email)
- [akshayjindal1990@gmail.com](email)
- yes. Please send it to ahbcdj@dkj.com

## intent:restaurant_search
- i'm looking for a place to eat
- I want to grab lunch
- I am looking out for some restaurants
- I’m hungry. Looking out for some good restaurants
- I’m hungry. Looking out for some good [chinese](cuisine) restaurants in [chandigarh](location)
- I am searching for a dinner spot
- I am looking for some restaurants in [Delhi](location).
- I am looking for some restaurants in [Bangalore](location)
- show me [chinese](cuisine) restaurants
- show me [chines](cuisine:chinese) restaurants in the [New Delhi](location:Delhi)
- show me a [mexican](cuisine) place in the [centre](location)
- i am looking for an [indian](cuisine) spot called olaolaolaolaolaola
- search for restaurants
- anywhere in the [west](location)
- I am looking for [asian fusion](cuisine) food
- I am looking a restaurant in [294328](location)
- in [Gurgaon](location)
- [South Indian](cuisine)
- [North Indian](cuisine)
- [Italian](cuisine)
- [Chinese](cuisine:chinese)
- [chinese](cuisine)
- [American](cuisine)
- [Mexican](cuisine)
- [Lithuania](location)
- Oh, sorry, in [Italy](location)
- in [delhi](location)
- I am looking for some restaurants in [Mumbai](location)
- I am looking for [mexican indian fusion](cuisine)
- can you book a table in [rome](location) in a [moderate](budgetmax:700.00) price range with [british](cuisine) food for [four](people:4) people
- [central](location) [indian](cuisine) restaurant
- please help me to find restaurants in [pune](location)
- Please find me a restaurantin [bangalore](location)
- [mumbai](location)
- show me restaurants
- please find me [chinese](cuisine) restaurant in [delhi](location)
- can you find me a [chinese](cuisine) restaurant
- [delhi](location)
- please find me a restaurant in [ahmedabad](location)
- please show me a few [italian](cuisine) restaurants in [bangalore](location)
- [300](budgetmin) - [700](budgetmax) range
- > [700](budgetmax)
- < [300](budgetmax)
- Lesser than Rs. [300](budgetmax)
- Rs. [300](budgetmin) to [700](budgetmax)
- More than [700](budgetmax)
- please show me [chinies](cuisine) restaurant in [Delhi](location) ranges between [300](budgetmin) to [500](budgetmax)
- Please find a restaurant
- [Delhi](location)
- Please find a restauramt
- Kollladdddjjjs
- [Kollkata](location)
- Looking for a restaurant
- [USA](location)
- between [300](budgetmin) to [700](budgetmax)
- Please show me a [chinies](cuisine) restaurant in [Delhi](location:delhi)
- Less than Rs. [300](budgetmax)
- Please show me a [chinies](cuisine) restaurant in [delhi](location) ranegs between [300](budgetmin) to [700](budgetmax)
- show me a [maxican](cuisine) restaurant in [Delhi](location:delhi)
- [Mexican](cuisine:mexican)
- Can you suggest some good restaurants in [Rishikesh](location)
- Okay. Show me some in [Allahabad](location)
- [bengaluru](location:bangalore)
- Can you suggest some good restaurants in [kolkata](location)
- in [mubaim](location)
- in [Mumbai](location:mumbai)
- please show me [chinies](cuisine) restaurant in [Delhi](location:delhi) ranges between [300](budgetmin) to [500](budgetmax)

## synonym:"Bokaro Steel City"
- Bokaro
- "Bokaro Steel City"

## synonym:"Durg-Bhilai Nagar"
- "Durg Nagar"
- "Bhilai Nagar"
- "Durg-Bhilai"

## synonym:"Hubli-Dharwad"
- Hubli
- Dharwad
- "Hubli-Darwad"

## synonym:"Vasai-Virar City"
- "Vasai City"
- "Virar City"
- "Vasai-Virar City"

## synonym:"north indian"
- "North Indian"
- "North indian"
- "north Indian"

## synonym:"south indian"
- "South Indian"
- "South indian"
- "south Indian"

## synonym:4
- four

## synonym:Bangalore
- bengaluru
- bangalore
- Bangalur

## synonym:Cuttack
- Cuttck
- Cuttak
- Cuttack

## synonym:Delhi
- New Delhi

## synonym:Firozabad
- Firozbad
- Firzabad
- Firozabad

## synonym:Kannur
- Kanur
- Kannur

## synonym:Kochi
- Cochi
- cochin
- Kochi

## synonym:Kottayam
- Kotayam
- Kottyam
- Kottayam

## synonym:North Indian
- north meal

## synonym:South Indian
- south meal

## synonym:Tiruchirappalli
- Tiruchirapalli
- Tiruchirappali
- Tiruchirapali

## synonym:Visakhapatnam
- Visakapatnam
- Vishakhapatnam
- Vishakapatnam

## synonym:agra
- Agra

## synonym:ahmedabad
- Ahmedabad
- Ahmedbad
- Ahmeadbad

## synonym:ajmer
- Ajmer
- ajmr
- ajmer

## synonym:aligarh
- alligarh
- Aligarh
- Aligadh

## synonym:allahabad
- Allahbad
- Alahabad
- Illahabad
- Prayagraj

## synonym:american
- American
- americn
- amrican

## synonym:amravati
- Amrvati
- Amravatti
- Amaravati

## synonym:amritsar
- amratsar
- amaritsar
- amrtsar

## synonym:asansol
- asnsol
- asansole
- asansol

## synonym:aurangabad
- aurangabad
- arangbad
- aurangabd

## synonym:bangalore
- Bengaluru

## synonym:bareilly
- barelly
- bareilly

## synonym:belgaum
- Belgam
- Belegaum
- Belegam

## synonym:bhavnagar
- bhavanagar
- Bhavnagar
- bhaavnagar

## synonym:bhiwandi
- bhiwaandi
- biwandi
- bhiwandi

## synonym:bhopal
- Bhopa
- Bhopl
- Bhopal

## synonym:bhubaneswar
- bhubaneswar
- Bhuvaneswar
- Bhubaneshwar

## synonym:bijapur
- Bijpur
- Bijapur

## synonym:bikaner
- Bikaner

## synonym:chandigarh
- Chandigarh
- Chandigadh
- Chandigar

## synonym:chennai
- chenai
- chenni
- madras

## synonym:chinese
- chines
- Chinese
- Chines

## synonym:coimbatore
- Coimbatore
- cbe
- Coimbator

## synonym:dehradun
- Dehradun
- Deharadun

## synonym:delhi
- Delhi
- "New Delhi"
- Delli
- Dilli

## synonym:dhanbad
- Dhanabad
- Dhanbd
- Dhanbad

## synonym:durgapur
- Duragapur
- Durgapure
- Durgapur

## synonym:erode
- Erode

## synonym:faridabad
- Faridabd
- Fardabad
- Faridabad

## synonym:ghaziabad
- Gaziabad
- Ghazibad
- Ghaziabad

## synonym:gorakhpur
- Gorakpur
- Gorkhpur
- Gorakhpur

## synonym:gulbarga
- Gulbrga
- Gulbarg
- Gulbarga

## synonym:guntur
- Guntr
- Gantur
- Guntur

## synonym:gurgaon
- Gurugaon
- Gurugram
- Gurgao

## synonym:guwahati
- Gowahati
- Guwahathi
- Guwahati

## synonym:gwalior
- Gwaliar
- Ghwaliar
- Gwalior

## synonym:hyderabad
- Hyderabad
- hyderbad
- hyderabd

## synonym:indore
- Indor
- Indore

## synonym:italian
- Italian
- Italin
- Itlian

## synonym:jabalpur
- jabalpur
- Jablpur
- Jabalpur

## synonym:jaipur
- Jaipure
- Jaypur
- Jaipur

## synonym:jalandhar
- Jalandhr
- Jalanadhar
- Jalandar

## synonym:jammu
- Jammu

## synonym:jamnagar
- Jamanagar
- Jamnagr
- Jamnagar

## synonym:jamshedpur
- jamshedpure
- Jamshedpur

## synonym:jhansi
- Jansi
- Jhanasi
- Jhansi

## synonym:jodhpur
- Jodpur
- Jodhpure
- Jodhpur

## synonym:kakinada
- Kakinda
- Kakinad
- Kakinada

## synonym:kanpur
- cawnpur
- Kaanpur
- Kanpur

## synonym:kolhapur
- Kolapur
- Kohlapur
- Kolhapur

## synonym:kolkata
- Kolkata
- Calcutta
- Kolkatta

## synonym:kollam
- Kolam
- Kollam

## synonym:kota
- Kota

## synonym:kozhikode
- Kozhikod
- Kozhikode

## synonym:kurnool
- Kurnol
- Koornool
- Kurnool

## synonym:lucknow
- Lucknow
- Lko
- Luknow

## synonym:ludhiana
- Ludiana
- Ludhiana

## synonym:madurai
- Madurai

## synonym:malappuram
- Malapuram
- Mallappuram
- Malappuram

## synonym:mangalore
- Mangalor
- Manglore
- Mangalore

## synonym:mathura
- Matura
- Mathura

## synonym:meerut
- Merut
- Meerut

## synonym:mexican
- Mexican
- Mexicn
- mxican

## synonym:mid
- moderate

## synonym:moradabad
- Muradabad
- Moradbad
- Moradabad

## synonym:mumbai
- Mumbai
- Bombai
- Bombay

## synonym:mysore
- mysore
- Mysor
- Mysure

## synonym:nagpur
- Nagpure
- Nagpor
- Nagpur

## synonym:nanded
- Nanded

## synonym:nashik
- Nasik
- Nashik

## synonym:nellore
- Nellor
- Nelore
- Nellore

## synonym:noida
- "greater Noida"
- "G. Noida"
- Noida

## synonym:palakkad
- Palakad
- Pallakkad
- Pallakad

## synonym:patna
- Patna

## synonym:pondicherry
- Puducherry
- Puducherri
- Pondi

## synonym:pune
- Pne
- Puna
- puna

## synonym:raipur
- Raipur

## synonym:rajahmundry
- Rajamundry
- Rajhmundry
- Rajmundry

## synonym:rajkot
- Rjakot
- Raajkot
- Rajkot

## synonym:ranchi
- Rancih
- Ranchi

## synonym:rourkela
- Rourkel
- Rorkela
- Rourekla

## synonym:salem
- Selam
- Selem
- Salem

## synonym:sangli
- sangli
- Sangil
- Sangli

## synonym:siliguri
- Siligur
- Siligudi
- Silguri

## synonym:solapur
- Sholapur
- Solpur
- Solapur

## synonym:srinagar
- Srinagr
- Sirinagar
- Sirnagar

## synonym:sultanpur
- Sultanapur
- Sulatanpur
- Sultanpur

## synonym:surat
- Surat

## synonym:thiruvananthapuram
- Trivandram
- Thiruvanantapuram
- Thiruvanthapuram

## synonym:thrissur
- Thirissur
- Thrisur
- Thirisur

## synonym:tirunelveli
- Trunelveli
- Thirunelveli
- Tirnelveli

## synonym:tiruppur
- Tirupur
- Tiruppor
- Tiruppur

## synonym:ujjain
- Ujain
- Ujjan
- Ujjain

## synonym:vadodara
- Baroda
- Vadodra
- Vadodara

## synonym:varanasi
- Varansi
- Banaras
- Varanasi

## synonym:vegetarian
- veggie
- vegg

## synonym:vijayawada
- Vijaywada
- Vijayawada
- Vijaiawada

## synonym:warangal
- Warangal

## regex:email
- ^[a-zA-Z0-9._%±]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,}$

## regex:greet
- hey[^\s]*

## regex:pincode
- [0-9]{6}

## lookup:location
  data/test/lookup_tables/location.txt
